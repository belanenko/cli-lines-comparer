package iterator

import (
	"bufio"
	"errors"
	"fmt"
	"os"
)

var (
	ErrFileNotExists = errors.New("file is not exists")
)

func ReadFile(path string) (<-chan string, error) {
	op := "iterator.ReadFile"

	linesC := make(chan string)

	f, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("failed to open file: %s: %w: %s", op, ErrFileNotExists, path)
	}

	go func() {
		defer close(linesC)
		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			line := scanner.Text()
			linesC <- line
		}
	}()

	return linesC, nil
}
