package iterator

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestReadFile(t *testing.T) {
	f, err := ioutil.TempFile("", "")
	require.NoError(t, err)

	lines := []string{"1", "2", "3"}

	for _, line := range lines {
		_, err := f.WriteString(line + "\n")
		require.NoError(t, err)
	}

	testCases := []struct {
		name  string
		path  string
		lines []string
		err   error
	}{
		{
			name:  "valid",
			path:  f.Name(),
			lines: lines,
			err:   nil,
		},
		{
			name:  "incorrect path",
			path:  "incorrect-path",
			lines: nil,
			// err: &os.PathError{
			// 	Op:   "open",
			// 	Path: "incorrect-path",
			// 	Err:  syscall.Errno,
			// },
			err: ErrFileNotExists,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			linesC, err := ReadFile(tc.path)
			if err != nil {
				assert.ErrorIs(t, err, tc.err)
				return
			}

			require.NotNil(t, linesC)

			lines := []string{}
			for line := range linesC {
				lines = append(lines, line)
			}

			assert.Equal(t, tc.lines, lines)
		})
	}
}
