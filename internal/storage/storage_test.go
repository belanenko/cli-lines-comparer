package storage

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAppend(t *testing.T) {
	testCases := []struct {
		name   string
		bucket string
		value  string
		err    error
	}{
		{
			name:   "valid",
			bucket: "example-bucket",
			value:  "example-value",
			err:    nil,
		},
		{
			name:   "empty bucket",
			bucket: "",
			value:  "example-value",
			err:    ErrEmptyBucketName,
		},
		{
			name:   "empty value",
			bucket: "example-bucket",
			value:  "",
			err:    ErrEmptyValue,
		},
	}

	storage := New()

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			count := len(storage.storage[tc.bucket])

			err := storage.Append(tc.bucket, tc.value)
			if tc.err != nil {
				assert.ErrorIs(t, err, tc.err)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, len(storage.storage), count+1, "count elements in stroage")
		})
	}
}

func TestExists(t *testing.T) {
	existsBucket := "exists-bucket"
	existsValue := "exists-value"

	testCases := []struct {
		name   string
		bucket string
		value  string
		exists bool
		err    error
	}{
		{
			name:   "valid",
			bucket: existsBucket,
			value:  existsValue,
			exists: true,
			err:    nil,
		},
		{
			name:   "unexists bucket",
			bucket: "unexists bucket",
			value:  "value",
			exists: false,
			err:    nil,
		},
		{
			name:   "unexists value",
			bucket: existsBucket,
			value:  "unexists value",
			exists: false,
			err:    nil,
		},
		{
			name:   "empty bucket name",
			bucket: "",
			exists: false,
			err:    ErrEmptyBucketName,
		},
		{
			name:   "empty value",
			bucket: existsBucket,
			exists: false,
			err:    ErrEmptyValue,
		},
	}

	storage := New()

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := storage.Append(existsBucket, existsValue)
			assert.NoError(t, err)

			exists, err := storage.Exists(tc.bucket, tc.value)
			assert.Equal(t, tc.err, err)

			assert.Equal(t, tc.exists, exists)
		})
	}
}
