package storage

import (
	"errors"
	"fmt"
)

var (
	ErrEmptyBucketName = errors.New("empty bucket name")
	ErrEmptyValue      = errors.New("empty value")
)

type Storage struct {
	storage map[string]map[string]struct{}
}

func New() *Storage {
	return &Storage{
		storage: make(map[string]map[string]struct{}),
	}
}

func (s *Storage) Append(bucket, value string) error {
	op := "storage.Append"

	if bucket == "" {
		return fmt.Errorf("failed to append: %s: %w", op, ErrEmptyBucketName)
	}

	if value == "" {
		return ErrEmptyValue
	}

	if _, exists := s.storage[bucket]; !exists {
		s.storage[bucket] = map[string]struct{}{}
	}
	s.storage[bucket][value] = struct{}{}

	return nil
}

func (s *Storage) Exists(bucket, value string) (bool, error) {
	op := "storage.Exists"

	if bucket == "" {
		return false, fmt.Errorf("%s: %w ", op, ErrEmptyBucketName)
	}

	if value == "" {
		return false, fmt.Errorf("%s: %w ", op, ErrEmptyValue)
	}

	v, exists := s.storage[bucket]
	if !exists {
		return false, nil
	}
	if _, exists := v[value]; !exists {
		return false, nil
	}
	return true, nil
}
