package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type arg struct {
	key   string
	value string
}

type args struct {
	args []arg
}

func (a *args) Array() []string {
	arr := []string{}
	for _, a := range a.args {
		arr = append(arr, a.key, a.value)
	}
	return arr
}

func TestLoad(t *testing.T) {

	testCases := []struct {
		name   string
		args   args
		config *Config
		err    error
	}{
		{
			name: "valid",
			args: args{
				[]arg{
					{
						key:   "-new-path",
						value: "someNewPath",
					},
					{
						key:   "-source-path",
						value: "someSourcePath",
					},
				},
			},
			config: &Config{
				NewPath:   "someNewPath",
				SoucePath: "someSourcePath",
			},
			err: nil,
		},
		{
			name: "empty args",
			args: args{
				[]arg{},
			},
			config: nil,
			err:    ErrFlagsIsNotSpecefied,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			cfg, err := Load(tc.args.Array())
			assert.Equal(t, tc.err, err)
			assert.Equal(t, tc.config, cfg)
		})
	}
}
