package config

import (
	"errors"
	"flag"
)

var (
	ErrFlagsIsNotSpecefied = errors.New("flags is not specefied")
)

type Config struct {
	NewPath   string
	SoucePath string
}

func Load(args []string) (*Config, error) {
	if len(args) == 0 {
		return nil, ErrFlagsIsNotSpecefied
	}
	flagSet := flag.NewFlagSet(args[0], flag.ExitOnError)

	pFlagNewPath := flagSet.String("new-path", "", "path to directory with new files")
	pFlagSourcePath := flagSet.String("source-path", "", "path to directory with source files")

	err := flagSet.Parse(args)
	if err != nil {
		return nil, err
	}

	return &Config{
		NewPath:   *pFlagNewPath,
		SoucePath: *pFlagSourcePath,
	}, nil
}
